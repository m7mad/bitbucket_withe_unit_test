﻿using System.Web;
using System.Web.Mvc;

namespace bitbucket_withe_unit_test
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
